#!/bin/bash
File="hosts"
Hosts=$(cat $File)
for Host in $Hosts
do	
	ssh-copy-id -i ~/.ssh/dorsal_pfd pidena@${Host}.info.polymtl.ca
done
