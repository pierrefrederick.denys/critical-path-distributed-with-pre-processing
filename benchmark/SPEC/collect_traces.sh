#!/bin/bash
File="hosts_reduced"
Hosts=$(cat $File)
Folder=$1
for Host in $Hosts
do	
	mkdir -p ./${1}/${Host}/
	scp -r -i ~/.ssh/dorsal_pfd pidena@${Host}.info.polymtl.ca:/home/tmp/tracing/${1}/${Host} ./${1}/${Host}
done
